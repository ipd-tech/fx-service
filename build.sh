#!/bin/sh

usage ()
{
  echo "Usage : build [ maven | dockerBuild | docker ]"
  echo "By default, when invoked without arguments, this script performs a maven build, i.e. as 'build.sh maven'"
  echo "'build.sh dockerBuild' - To build a docker image and publish to the local docker daemon"
  echo "'build.sh docker'      - To build a docker image and publish to ECR"
  exit
}

build_mode="maven"

if [ -z $1 ]
then
  echo "No arguments passed, defaulting to build mode to 'maven'"
  echo "For usage, run 'build.sh help'"
elif [ -n $1 ]
then
  build_mode=$1
fi

case $build_mode in
   "maven")
        mvn clean package -DskipTests
        ;;
   "dockerBuild")
        mvn clean package -DskipTests jib:dockerBuild
        ;;
   "docker")
        mvn clean package -DskipTests jib:build
        ;;
   "help" | "usage" | "?" | "--?")
        usage
        ;;
    *)
        echo "Invalid argument... "
        usage
        ;;
esac
