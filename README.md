
# FX-Service

### Building and Running the FX-Service

* `git clone` this repo & `cd fx-service`
* Build using `mvn clean package -DskipTests`
* Run using `java -jar target/fx-service-0.0.1-SNAPSHOT.jar`
* The `dev` spring profile is active by default
* To switch application profiles, run `java -jar -Dspring.profiles.active=[dev|prod] target/fx-service-0.0.1-SNAPSHOT.jar` 

### Docker Images - Building and Running using Jib Plugin

This project uses [jib-maven-plugin](https://github.com/GoogleContainerTools/jib/tree/master/jib-maven-plugin) to build and publish Docker images

#### Pre-requisities
* `Docker Credential Helper` - To publish the docker image built to a private Docker Hub Registry or ECS Registry, a local setup docker credential helper is needed.
  * See here for [ECS Credential Helper](https://github.com/awslabs/amazon-ecr-credential-helper)

#### To build the Docker image and to publish -

* Once the credential helper for `docker-credential-ecr-login` is available in the `$PATH`, to build the image and publish 
  * To the local docker daemon, run `mvn clean package -DskipTests jib:dockerBuild`
  * To the ECR registry, run `mvn clean package -DskipTests jib:build`

#### To build the Docker image and to publish -

  * To view the `Dockerfile` created by the jib plugin to build the image, run `mvn jib:exportDockerContext` and `jib` exports the `Dockerfile` to `target\jib-docker-context` directory
  
#### To run the Docker image -

  * `docker run -p 5555:5555 760360180409.dkr.ecr.us-east-1.amazonaws.com/fx-service`
  
### The FX-Service module serves 2 purposes -

* Scheduled Cron to Fetch FX Rates
* REST API for getting / updating FX rates

### Scheduled Cron to Fetch FX Rates 

* Periodically fetches the latest FX Rates based on [Currency Layer API]([https://currencylayer.com/documentation) and updates them to the `TIPS DB`
* For FX rates refresh schedules, check the `rates.refresh.cron` property value from the `application.yaml` file
* The property `currency-layer.config.currencyKeys` determines the list of currencies for which FX Rates are updated
* The `spring.profiles.active` property determines which `TIPS DB` is updated

### FX-Service REST API 

The REST API supports the below endpoints -

------
**NOTE:**  If the below requests are run - 

* In Dev environment, the port is 5555. The host part of the endpoint URL should be http://localhost:5555/
* In Prod environment, the port is 8955. The host part of the endpoint URL should be http://localhost:8955/

------

* `/latestAccFxRate?from=<>&to=<>` - `GET` - Provides latest currency rates for a given `from` currency to `to` currency. Currently, only `to` currency of `USD` is supported.
```bash
   $ curl "http://localhost:8955/tips-currencyservice/api/currencyrateservice/latestAccFxRate?from=CAD&to=USD"
```
```json
   {
     "idAccFxRate": 1002595,
     "dateEff": "2018-10-25T07:00:00.000+0000",
     "dateTerm": "2018-10-25T07:00:00.000+0000",
     "rate": 0.7646,
     "currencyFrom": "CAD",
     "currencyTo": "USD"
   }
```


* `/accFxRate/{idAccFxRate}` - `GET` - Get currency for a given `ID`
```bash
   $ curl "http://localhost:8955/tips-currencyservice/api/currencyrateservice/accFxRate/1002595"
```
```json
   {
     "idAccFxRate": 1002595,
     "dateEff": "2018-10-25T07:00:00.000+0000",
     "dateTerm": "2018-10-25T07:00:00.000+0000",
     "rate": 0.7646,
     "currencyFrom": "CAD",
     "currencyTo": "USD"
   }
```

* `/saveAccFxRate` - `POST` - Saves or updates the FX Rate Info provided

```bash
   $ curl -H "Content-Type:application/json" http://localhost:8955/tips-currencyservice/api/currencyrateservice/saveAccFxRate -d \
     "{ \
         \"dateEff\" : \"2018-10-25T07:00:00.000+0000\", \
         \"dateTerm\" : \"2018-10-25T07:00:00.000+0000\", \
         \"rate\" : 0.99, \
         \"currencyFrom\" : \"CAD\", \
         \"currencyTo\" : \"USD\" \
     }"
```
```json  
     {
       "idAccFxRate": 1002595,
       "dateEff": "2018-10-25T07:00:00.000+0000",
       "dateTerm": "2018-10-25T07:00:00.000+0000",
       "rate": 0.99,
       "currencyFrom": "CAD",
       "currencyTo": "USD"
     }
```

* `/updateAccFxRateForDate?date=<>` - `GET` - Creates or Updates the FX Rate Info for a given `date`. The `date` parameter should be in the format `yyyy-mm-dd`
```bash
   $ curl "http://localhost:8955/tips-currencyservice/api/currencyrateservice/updateAccFxRateForDate?date=2018-10-25"
```
```json
     {
       "NZD=>USD": "Successfully Updated",
       "AUD=>USD": "Successfully Updated",
       "CAD=>USD": "Successfully Updated",
       "GBP=>USD": "Successfully Updated"
     }
```
* `/save` - `POST` - alias for `/saveAccFxRate`
