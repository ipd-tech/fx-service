#!/bin/bash

SERVICE_NAME="fx-service"

AWS_ECR="760360180409.dkr.ecr.us-east-1.amazonaws.com"
AWS_ECR_REPO="fx-service"

usage ()
{
  echo "Usage : run [ dev | prod | docker ]"
  echo "By default, when invoked without arguments, this script runs the $SERVICE_NAME locally as a spring boot application with env set to 'dev', as 'run.sh dev'"
  echo "'run.sh [dev]'        - To run $SERVICE_NAME locally as a spring boot application with spring active profile set to 'dev'"
  echo "'run.sh prod'         - To run $SERVICE_NAME locally as a spring boot application with spring active profile set to 'prod'"
  echo "'run.sh docker [dev]' - To run $SERVICE_NAME as a spring boot application within a docker container with spring active profile set to 'dev'"
  echo "'run.sh docker prod'  - To run $SERVICE_NAME as a spring boot application within a docker container with spring active profile set to 'prod'"
  exit
}

runInDocker() {
    docker_env="dev"

    if [ -z $1 ]
    then
      echo "No arguments passed, defaulting to spring active profile 'dev'. To see usage, execute 'run.sh usage'"
    elif [ -n $1 ]
    then
      docker_env=$1
    fi

    case $docker_env in
           "dev")
                echo "Starting the docker container application with spring.profiles.active='$docker_env' and on port 5555"
                docker run -p 5555:5555 $AWS_ECR/$AWS_ECR_REPO
                ;;
           "prod")
                echo "Starting the docker container application with spring.profiles.active='$docker_env' and on port 8955"
                docker run -e "SPRING_PROFILES_ACTIVE=prod" -p 8955:8955 $AWS_ECR/$AWS_ECR_REPO
                ;;
           *)
                echo "Sorry, Unknown spring profile!"
                usage
                ;;
    esac
}

env="dev"

if [ -z $1 ]
then
  echo "No arguments passed, defaulting to spring active profile 'dev'. To see usage, execute 'run.sh usage'"
elif [ -n $1 ]
then
  env=$1
fi

case $env in
   "dev")
        echo "Starting the application with spring.profiles.active='$env' and on port 5555"
        mvn spring-boot:run
        ;;
   "prod")
        echo "Starting the application with spring.profiles.active='$env' and on port 8955"
        mvn spring-boot:run -Dspring.profiles.active=prod
        ;;
   "docker")
        runInDocker $2
        ;;
   *)
        echo "Sorry, Unknown spring profile!"
        usage
        ;;
esac
