package com.idp.tips.fxservice.util;

import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@Component
public class DateUtil {
	
	private static final String DATE_FORMAT = "yyyy-MM-dd";

	public static Date parseDate(String dateStr) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		return formatter.parse(dateStr);
	}

	public static String formatDate(Date date){
		if(date == null) return null;
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		return formatter.format(date);
	}
	/**
	 * Converts the given <code>date</code> from the <code>fromTimeZone</code> to the
	 * <code>toTimeZone</code>.  Since java.util.Date has does not really store time zome
	 * information, this actually converts the date to the date that it would be in the
	 * other time zone.
	 * @param date
	 * @param fromTimeZone
	 * @param toTimeZone
	 * @return
	 */
	public Date convertTimeZone(Date date, TimeZone fromTimeZone, TimeZone toTimeZone)
	{
	    long fromTimeZoneOffset = getTimeZoneUTCAndDSTOffset(date, fromTimeZone);
	    long toTimeZoneOffset = getTimeZoneUTCAndDSTOffset(date, toTimeZone);

	    return new Date(date.getTime() + (toTimeZoneOffset - fromTimeZoneOffset));
	}

	/**
	 * Calculates the offset of the <code>timeZone</code> from UTC, factoring in any
	 * additional offset due to the time zone being in daylight savings time as of
	 * the given <code>date</code>.
	 * @param date
	 * @param timeZone
	 * @return
	 */
	private long getTimeZoneUTCAndDSTOffset(Date date, TimeZone timeZone)
	{
	    long timeZoneDSTOffset = 0;
	    if(timeZone.inDaylightTime(date))
	    {
	        timeZoneDSTOffset = timeZone.getDSTSavings();
	    }

	    return timeZone.getRawOffset() + timeZoneDSTOffset;
	}
	
	public Date getEndOfDay(Date date) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 999);
	    return calendar.getTime();
	}

	public Date getStartOfDay(Date date) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    return calendar.getTime();
	}

	public static boolean isDateValid(String date)
	{
		try {
			DateFormat df = new SimpleDateFormat(DATE_FORMAT);
			df.setLenient(false);
			df.parse(date);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}
}
