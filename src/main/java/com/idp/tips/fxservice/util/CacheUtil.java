package com.idp.tips.fxservice.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import com.idp.tips.fxservice.TipsDbServiceConstant;

@Component
public class CacheUtil {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	/**
	 * Through usage of annotations all of the managed caches will be cleared.
	 */
	@Caching(evict={
			@CacheEvict(value=TipsDbServiceConstant.CACHE_LATESTACCFXRATE, allEntries=true),
			@CacheEvict(value=TipsDbServiceConstant.CACHE_ACCFXRATE, allEntries=true)
	})
	public void clearAllCaches() {
		logger.info("All caches have been completely cleared");
	}
	
	@CacheEvict(value=TipsDbServiceConstant.CACHE_LATESTACCFXRATE, allEntries=true)
	public void clearLatestRateCaches() {
		logger.info("LatestCurrencyRate caches have been completely cleared");
	}
}
