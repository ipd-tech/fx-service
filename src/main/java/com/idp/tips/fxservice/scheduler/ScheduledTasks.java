package com.idp.tips.fxservice.scheduler;

import com.idp.tips.fxservice.util.CacheUtil;
import com.idp.tips.fxservice.web.service.IAccFxRateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

@Component
@EnableScheduling
public class ScheduledTasks {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IAccFxRateService accFxRateService;

	@Autowired
	private CacheUtil cacheUtil;

	@Bean
	public TaskScheduler taskScheduler() {
		return new ConcurrentTaskScheduler(); //single threaded by default
	}
	
	@Scheduled(cron = "${cache.clean.cron}")
	public void cleanCacheTask() {
		logger.info("Task clear all caches has started at :" + new Date());
		cacheUtil.clearAllCaches();
	}

	@Scheduled(cron = "${rates.refresh.cron}")
	public void processTask() {
		logger.info("Task currency rates refresh from external sources has started at :" + new Date());
		Map<String, Boolean> updateStatus = accFxRateService.updateAccFxRates();
		logger.info("updateStatus = " + updateStatus);
		logger.info("Task currencyExchange finished time:" + new Date());
	}

}
