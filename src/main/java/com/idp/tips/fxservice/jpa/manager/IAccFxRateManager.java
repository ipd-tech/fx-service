package com.idp.tips.fxservice.jpa.manager;

import com.idp.tips.fxservice.dto.AccFxRateDTO;

public interface IAccFxRateManager {

    AccFxRateDTO findLatestRate(String currencyFrom, String currencyTo);
    AccFxRateDTO findAccFxRate(Integer idAccFxRate);
    AccFxRateDTO saveOrUpdate(AccFxRateDTO accFxRateDto);
}
