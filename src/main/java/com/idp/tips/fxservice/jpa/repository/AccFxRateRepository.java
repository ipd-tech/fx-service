package com.idp.tips.fxservice.jpa.repository;

import com.idp.tips.fxservice.jpa.entity.AccFxRate;
import com.idp.tips.fxservice.jpa.entity.RefCurrency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface AccFxRateRepository extends JpaRepository<AccFxRate, Integer> {
	
	List<AccFxRate> findByCurrencyFromAndCurrencyTo(RefCurrency currencyFrom, RefCurrency currencyTo);
	
	AccFxRate findTopByCurrencyFromAndCurrencyToOrderByDateEffDesc(RefCurrency currencyFrom, RefCurrency currencyTo);

	AccFxRate findByCurrencyFromAndCurrencyToAndDateEff(RefCurrency currencyFrom, RefCurrency currencyTo, Date dateEff);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update AccFxRate accFxRate set accFxRate.rate=:rateArg,accFxRate.dateTerm=:dateTermArg where accFxRate.idAccFxRate=:idAccFxRateArg")
	int updateAccFxRate(@Param("idAccFxRateArg") Integer idAccFxRate, @Param("rateArg") double rate, @Param("dateTermArg") Date dateTerm);

}
