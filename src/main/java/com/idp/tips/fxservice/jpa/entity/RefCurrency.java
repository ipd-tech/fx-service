/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.idp.tips.fxservice.jpa.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author ahg
 */
@Entity
@Table(name = "RefCurrency")
@NamedQueries({
    @NamedQuery(name = "RefCurrency.findAll", query = "SELECT r FROM RefCurrency r")})
public class RefCurrency implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "currency")
    private String currency;
    @Basic(optional = false)
    @Column(name = "idRefCurrency")
    private int idRefCurrency;
    @Basic(optional = false)
    @Column(name = "currencyName")
    private String currencyName;
    @Column(name = "symbol")
    private Character symbol;
    @Column(name = "prefix")
    private String prefix;
    @Basic(optional = false)
    @Lob
    @Column(name = "rowVersion")
    private byte[] rowVersion;
    @Column(name = "shortFormat")
    private String shortFormat;
    @Column(name = "longFormat")
    private String longFormat;
    @Basic(optional = false)
    @Column(name = "isSpecialCurrency")
    private boolean isSpecialCurrency;
    @Column(name = "decimalDigits")
    private Integer decimalDigits;
    @Column(name = "isDynamicCurrency")
    private Boolean isDynamicCurrency;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "currencyFrom")
//    private List<AccFxRate> accFxRateList;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "currencyTo")
//    private List<AccFxRate> accFxRateList1;
//    @OneToMany(mappedBy = "currency")
//    private List<RefCountry> refCountryList;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "currency")
//    private List<UwSpecialCurrencyRate> uwSpecialCurrencyRateList;
//    @OneToMany(mappedBy = "specialCurrency")
//    private List<RefClmType> refClmTypeList;
//    @OneToMany(mappedBy = "specialCurrency")
//    private List<UwProd> uwProdList;

    public RefCurrency() {
    }

    public RefCurrency(String currency) {
        this.currency = currency;
    }

    public RefCurrency(String currency, int idRefCurrency, String currencyName, byte[] rowVersion, boolean isSpecialCurrency) {
        this.currency = currency;
        this.idRefCurrency = idRefCurrency;
        this.currencyName = currencyName;
        this.rowVersion = rowVersion;
        this.isSpecialCurrency = isSpecialCurrency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getIdRefCurrency() {
        return idRefCurrency;
    }

    public void setIdRefCurrency(int idRefCurrency) {
        this.idRefCurrency = idRefCurrency;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Character getSymbol() {
        return symbol;
    }

    public void setSymbol(Character symbol) {
        this.symbol = symbol;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public byte[] getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(byte[] rowVersion) {
        this.rowVersion = rowVersion;
    }

    public String getShortFormat() {
        return shortFormat;
    }

    public void setShortFormat(String shortFormat) {
        this.shortFormat = shortFormat;
    }

    public String getLongFormat() {
        return longFormat;
    }

    public void setLongFormat(String longFormat) {
        this.longFormat = longFormat;
    }

    public boolean getIsSpecialCurrency() {
        return isSpecialCurrency;
    }

    public void setIsSpecialCurrency(boolean isSpecialCurrency) {
        this.isSpecialCurrency = isSpecialCurrency;
    }

    public Integer getDecimalDigits() {
        return decimalDigits;
    }

    public void setDecimalDigits(Integer decimalDigits) {
        this.decimalDigits = decimalDigits;
    }

    public Boolean getIsDynamicCurrency() {
        return isDynamicCurrency;
    }

    public void setIsDynamicCurrency(Boolean isDynamicCurrency) {
        this.isDynamicCurrency = isDynamicCurrency;
    }

//    public List<AccFxRate> getAccFxRateList() {
//        return accFxRateList;
//    }
//
//    public void setAccFxRateList(List<AccFxRate> accFxRateList) {
//        this.accFxRateList = accFxRateList;
//    }
//
//    public List<AccFxRate> getAccFxRateList1() {
//        return accFxRateList1;
//    }
//
//    public void setAccFxRateList1(List<AccFxRate> accFxRateList1) {
//        this.accFxRateList1 = accFxRateList1;
//    }
//
//    public List<RefCountry> getRefCountryList() {
//        return refCountryList;
//    }
//
//    public void setRefCountryList(List<RefCountry> refCountryList) {
//        this.refCountryList = refCountryList;
//    }
//
//    public List<UwSpecialCurrencyRate> getUwSpecialCurrencyRateList() {
//        return uwSpecialCurrencyRateList;
//    }
//
//    public void setUwSpecialCurrencyRateList(List<UwSpecialCurrencyRate> uwSpecialCurrencyRateList) {
//        this.uwSpecialCurrencyRateList = uwSpecialCurrencyRateList;
//    }
//
//    public List<RefClmType> getRefClmTypeList() {
//        return refClmTypeList;
//    }
//
//    public void setRefClmTypeList(List<RefClmType> refClmTypeList) {
//        this.refClmTypeList = refClmTypeList;
//    }
//
//    public List<UwProd> getUwProdList() {
//        return uwProdList;
//    }
//
//    public void setUwProdList(List<UwProd> uwProdList) {
//        this.uwProdList = uwProdList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (currency != null ? currency.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefCurrency)) {
            return false;
        }
        RefCurrency other = (RefCurrency) object;
        if ((this.currency == null && other.currency != null) || (this.currency != null && !this.currency.equals(other.currency))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.idp.tips.fxservice.entity.RefCurrency[ currency=" + currency + " ]";
    }
    
}
