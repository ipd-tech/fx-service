package com.idp.tips.fxservice.jpa.repository;

import com.idp.tips.fxservice.jpa.entity.RefCurrency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface RefCurrencyRepository extends JpaRepository<RefCurrency, String> {
	@Modifying
	@Query(value = "SET IDENTITY_INSERT REFCURRENCY ON", nativeQuery = true)
	public void enableInsertIdentity();
}
