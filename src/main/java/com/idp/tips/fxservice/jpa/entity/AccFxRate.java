/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.idp.tips.fxservice.jpa.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author ahg
 */
@Entity
@Table(name = "AccFxRate")

public class AccFxRate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idAccFxRate")
    private Integer idAccFxRate;
    @Column(name = "dateEff")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEff;
    @Column(name = "dateTerm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTerm;
    @Basic(optional = false)
    @Column(name = "rate")
    private double rate;
    @Basic(optional = false)
    @Lob
    @Column(name = "rowVersion", insertable = false)
    private byte[] rowVersion;

    @JoinColumn(name = "currencyFrom", referencedColumnName = "currency")
    @ManyToOne(optional = false)
    private RefCurrency currencyFrom;
    @JoinColumn(name = "currencyTo", referencedColumnName = "currency")
    @ManyToOne(optional = false)
    private RefCurrency currencyTo;
//    @OneToMany(mappedBy = "specialCurrencyIdAccFxRate")
//    private List<Clm> clmList;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAccFxRate")
//    private List<UwSpecialCurrencyRate> uwSpecialCurrencyRateList;
//    @OneToMany(mappedBy = "idAccFxRatePrem")
//    private List<UwSpecialCurrencyRate> uwSpecialCurrencyRateList1;
//    @OneToMany(mappedBy = "specialCurrencyIdAccFxRate")
//    private List<Pol> polList;

    public AccFxRate() {
    }

    public AccFxRate(Integer idAccFxRate) {
        this.idAccFxRate = idAccFxRate;
    }

    public AccFxRate(Integer idAccFxRate, double rate, byte[] rowVersion) {
        this.idAccFxRate = idAccFxRate;
        this.rate = rate;
        this.rowVersion = rowVersion;
    }

    public Integer getIdAccFxRate() {
        return idAccFxRate;
    }

    public void setIdAccFxRate(Integer idAccFxRate) {
        this.idAccFxRate = idAccFxRate;
    }

    public Date getDateEff() {
        return dateEff;
    }

    public void setDateEff(Date dateEff) {
        this.dateEff = dateEff;
    }

    public Date getDateTerm() {
        return dateTerm;
    }

    public void setDateTerm(Date dateTerm) {
        this.dateTerm = dateTerm;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public byte[] getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(byte[] rowVersion) {
        this.rowVersion = rowVersion;
    }

    public RefCurrency getCurrencyFrom() {
        return currencyFrom;
    }

    public void setCurrencyFrom(RefCurrency currencyFrom) {
        this.currencyFrom = currencyFrom;
    }

    public RefCurrency getCurrencyTo() {
        return currencyTo;
    }

    public void setCurrencyTo(RefCurrency currencyTo) {
        this.currencyTo = currencyTo;
    }
//
//    public List<Clm> getClmList() {
//        return clmList;
//    }
//
//    public void setClmList(List<Clm> clmList) {
//        this.clmList = clmList;
//    }
//
//    public List<UwSpecialCurrencyRate> getUwSpecialCurrencyRateList() {
//        return uwSpecialCurrencyRateList;
//    }
//
//    public void setUwSpecialCurrencyRateList(List<UwSpecialCurrencyRate> uwSpecialCurrencyRateList) {
//        this.uwSpecialCurrencyRateList = uwSpecialCurrencyRateList;
//    }
//
//    public List<UwSpecialCurrencyRate> getUwSpecialCurrencyRateList1() {
//        return uwSpecialCurrencyRateList1;
//    }
//
//    public void setUwSpecialCurrencyRateList1(List<UwSpecialCurrencyRate> uwSpecialCurrencyRateList1) {
//        this.uwSpecialCurrencyRateList1 = uwSpecialCurrencyRateList1;
//    }
//
//    public List<Pol> getPolList() {
//        return polList;
//    }
//
//    public void setPolList(List<Pol> polList) {
//        this.polList = polList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAccFxRate != null ? idAccFxRate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccFxRate)) {
            return false;
        }
        AccFxRate other = (AccFxRate) object;
        if ((this.idAccFxRate == null && other.idAccFxRate != null) || (this.idAccFxRate != null && !this.idAccFxRate.equals(other.idAccFxRate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.idp.tips.fxservice.entity.AccFxRate[ idAccFxRate=" + idAccFxRate + " ]";
    }
    
}
