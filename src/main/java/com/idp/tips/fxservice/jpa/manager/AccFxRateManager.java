package com.idp.tips.fxservice.jpa.manager;

import com.idp.tips.fxservice.dto.AccFxRateDTO;
import com.idp.tips.fxservice.jpa.entity.AccFxRate;
import com.idp.tips.fxservice.jpa.entity.RefCurrency;
import com.idp.tips.fxservice.jpa.repository.AccFxRateRepository;
import com.idp.tips.fxservice.jpa.repository.RefCurrencyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AccFxRateManager implements IAccFxRateManager {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AccFxRateRepository accFxRateRepository;

	@Autowired
	private RefCurrencyRepository refCurrencyRepository;

	// @Override
	public AccFxRateDTO findLatestRate(String currencyFrom, String currencyTo) {
		logger.debug("calling findLatestRate in AccFxRateManager.");
		RefCurrency fromCurrency = refCurrencyRepository.getOne(currencyFrom);
		RefCurrency toCurrency = refCurrencyRepository.getOne(currencyTo);

		AccFxRate accFxRate = accFxRateRepository.findTopByCurrencyFromAndCurrencyToOrderByDateEffDesc(fromCurrency,
				toCurrency);
		return accFxRate == null? null: new AccFxRateDTO(accFxRate.getIdAccFxRate(), accFxRate.getDateEff(), accFxRate.getDateTerm(),
				accFxRate.getRate(), accFxRate.getCurrencyFrom().getCurrency(),
				accFxRate.getCurrencyTo().getCurrency());
	}

	public AccFxRateDTO saveOrUpdate(AccFxRateDTO accFxRateDto) {

		RefCurrency fromCurrency = refCurrencyRepository.getOne(accFxRateDto.getCurrencyFrom());
		RefCurrency toCurrency = refCurrencyRepository.getOne(accFxRateDto.getCurrencyTo());

		AccFxRate dbMatch = accFxRateRepository.findByCurrencyFromAndCurrencyToAndDateEff(fromCurrency, toCurrency, accFxRateDto.getDateEff());

		Integer accFxRateId = null;
		if(dbMatch != null) {
			Integer dbMatchIdAccFxRate = dbMatch.getIdAccFxRate();
			int updated = accFxRateRepository.updateAccFxRate(dbMatchIdAccFxRate, accFxRateDto.getRate(), accFxRateDto.getDateTerm());
			if(updated == 1) {
				accFxRateId = dbMatchIdAccFxRate;
			}
		} else {
			AccFxRate newRate = new AccFxRate();
			newRate.setCurrencyFrom(fromCurrency);
			newRate.setCurrencyTo(toCurrency);
			newRate.setDateEff(accFxRateDto.getDateEff());
			newRate.setDateTerm(accFxRateDto.getDateTerm());
			newRate.setRate(accFxRateDto.getRate());
			AccFxRate accFxRate = accFxRateRepository.saveAndFlush(newRate);
			accFxRateId = accFxRate.getIdAccFxRate();
		}

		if(accFxRateId == null) return null;

		accFxRateDto.setIdAccFxRate(accFxRateId);
		return accFxRateDto;
	}

	public AccFxRateDTO findAccFxRate(Integer idAccFxRate) {
		logger.info("calling findAccFxRate in AccFxRateManager.");
		Optional<AccFxRate> accFxRateRef = accFxRateRepository.findById(idAccFxRate);
		if (!accFxRateRef.isPresent())
			return null;

		AccFxRate accFxRate = accFxRateRef.get();
		return accFxRate == null? null: new AccFxRateDTO(accFxRate.getIdAccFxRate(), accFxRate.getDateEff(), accFxRate.getDateTerm(),
				accFxRate.getRate(), accFxRate.getCurrencyFrom().getCurrency(),
				accFxRate.getCurrencyTo().getCurrency());
	}

}
