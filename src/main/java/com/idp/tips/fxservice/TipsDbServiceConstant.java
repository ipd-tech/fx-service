package com.idp.tips.fxservice;

public interface TipsDbServiceConstant {
	public static final String CACHE_LATESTACCFXRATE = "latestAccFxRate";
	public static final String CACHE_ACCFXRATE = "accFxRate";
}
