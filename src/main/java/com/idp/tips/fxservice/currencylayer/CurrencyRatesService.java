package com.idp.tips.fxservice.currencylayer;

import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class CurrencyRatesService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    CurrencyRatesServiceConfig config;

    @Autowired
    RestTemplate restTemplate;


    public Map<String, Double> getCurrencyRates() {
        return getCurrencyRates(null);
    }

    public Map<String, Double> getCurrencyRates(Date currencyDate) {
        Map<String, Double> fxRates = new HashMap<>();

        String currencyLayerUrl = config.getFxRatesApiEndpoint(currencyDate);
        logger.info("Currency Layer URL: " + currencyLayerUrl + "\n");

        try {
            logger.info("Getting rates for " + currencyDate);
            ResponseEntity<String> response = restTemplate.getForEntity(currencyLayerUrl, String.class);
            JSONObject exchangeRates = new JSONObject(response.getBody());
            logger.debug("Live Currency Exchange Rates: " + exchangeRates);

            JSONObject quotes = exchangeRates.getJSONObject("quotes");

            Map<String, String> currencyKeys = config.getCurrencyKeys();

            if(currencyKeys.size() != quotes.length()) {
                throw new IllegalStateException("The no. of currencies for which FX rates requested and received are not matching.. ");
            }

            for(String currency : currencyKeys.keySet()) {
                // per currency layer API, when we ask the conversion rate for a currency "CAD",
                // the response will be in the form "USDCAD" : "<VALUE>", hence responseKey = USDCAD
                String responseKey = currencyKeys.get(currency);

                double exchangeRateFromUSD = quotes.getDouble(responseKey);
                logger.debug("1 USD = " + exchangeRateFromUSD + " " + currency);

                double exchangeRateToUSD = round((1/exchangeRateFromUSD),4); // 1 [CAD|GBP|AUD|NZD] = x USD
                logger.debug("1 " + currency + " = " + exchangeRateToUSD + " USD");

                fxRates.put(currency, exchangeRateToUSD);
            }

            logger.info("FX Rates (to USD) = " + fxRates);

        } catch (ParseException e) {
            logger.error("Response parse exception - " + e.getMessage());
            throw e;
        } catch (JSONException e) {
            logger.error("JSON Response exception - " + e.getMessage());
            throw e;
        }

        return fxRates;
    }

    /**
     * Reference - https://stackoverflow.com/a/2808648/829542
     * @param value to be rounded
     * @param places
     * @return
     */
    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
