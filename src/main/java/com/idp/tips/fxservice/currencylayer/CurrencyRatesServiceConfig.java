package com.idp.tips.fxservice.currencylayer;

import com.idp.tips.fxservice.util.DateUtil;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

@ConfigurationProperties(prefix = "currency-layer.config")
@Configuration
public class CurrencyRatesServiceConfig {

    private String serviceUrl;

    private Map<String, String> currencyKeys;

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public void setCurrencyKeys(Map<String, String> currencyKeys) {
        this.currencyKeys = currencyKeys;
    }

    public Map<String, String> getCurrencyKeys() {
        return currencyKeys;
    }

    public String getFxRatesApiEndpoint(Date currencyDate) {
        String keys = currencyKeys.keySet().stream().collect(Collectors.joining(","));
        if (currencyDate != null) {
            return serviceUrl + keys + "&date=" + DateUtil.formatDate(currencyDate);
        }
        return serviceUrl + keys;
    }

    public String getFxRatesApiEndpoint() {
        return getFxRatesApiEndpoint(null);
    }

}
