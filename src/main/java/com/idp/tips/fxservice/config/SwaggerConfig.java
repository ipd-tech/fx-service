package com.idp.tips.fxservice.config;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * config for swagger
 * @author ahg
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.any())              
          //.paths(PathSelectors.any())  
          .paths(paths())
          .build()
          .apiInfo(apiInfo());                                           
    }

	private ApiInfo apiInfo() {
		ApiInfo apiInfo = new ApiInfo(
				"CurrencyRate Service REST API",
				"Rest API for CurrencyRate Service.",
				"1.0",
				"Terms of service",
				new Contact("CSA Travel Protection", "http://csatravelprotection.com", "Ahuang@csatravelprotection.com"),
				"License of API",
				"https://www.apache.org/licenses/LICENSE-2.0");
		return apiInfo;
	}

	@SuppressWarnings("unchecked")
	private Predicate<String> paths() {
		return or(
				regex("/api/*.*"));
	}
}
