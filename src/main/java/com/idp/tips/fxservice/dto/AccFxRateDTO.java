package com.idp.tips.fxservice.dto;

import java.io.Serializable;
import java.util.Date;

public final class AccFxRateDTO implements Serializable {
    private Integer idAccFxRate;
    private Date dateEff;
    private Date dateTerm;
    private double rate;
    private String currencyFrom;
    private String currencyTo;

    public AccFxRateDTO() {
    }

    public AccFxRateDTO(Integer idAccFxRate, Date dateEff, Date dateTerm, double rate, String currencyFrom, String currencyTo) {
        this.idAccFxRate = idAccFxRate;
        this.dateEff = dateEff;
        this.dateTerm = dateTerm;
        this.rate = rate;
        this.currencyFrom = currencyFrom;
        this.currencyTo = currencyTo;
    }

    public void setIdAccFxRate(Integer idAccFxRate) {
        this.idAccFxRate = idAccFxRate;
    }

    public Integer getIdAccFxRate() {
        return this.idAccFxRate;
    }

    public Date getDateEff() {
        return this.dateEff;
    }

    public Date getDateTerm() {
        return this.dateTerm;
    }

    public double getRate() {
        return this.rate;
    }

    public String getCurrencyFrom() {
        return this.currencyFrom;
    }

    public String getCurrencyTo() {
        return this.currencyTo;
    }

    public String toString() {
        return "AccFxRateDTO [idAccFxRate=" + this.idAccFxRate + ", dateEff=" + this.dateEff + ", dateTerm=" + this.dateTerm + ", rate=" + this.rate + ", currencyFrom=" + this.currencyFrom + ", currencyTo=" + this.currencyTo + "]";
    }
}
