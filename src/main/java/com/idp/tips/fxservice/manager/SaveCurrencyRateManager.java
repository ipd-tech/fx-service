package com.idp.tips.fxservice.manager;

import com.idp.tips.fxservice.dto.AccFxRateDTO;
import com.idp.tips.fxservice.util.DateUtil;
import com.idp.tips.fxservice.web.service.IAccFxRateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * this class is to save currency rate to db by calling currency rate service
 * @author ahg
 *
 */
@Component
public class SaveCurrencyRateManager {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final String currencyTo = "USD";
	
	@Autowired
	private DateUtil dateUtil;

	@Autowired
	private IAccFxRateService accFxRateService;

	public Map<String, Boolean> saveCurrencyRatesFromMap(Map<String, Double> currencyRates) {
		return saveCurrencyRatesFromMap(currencyRates, new Date());
	}

	public Map<String, Boolean> saveCurrencyRatesFromMap(Map<String, Double> currencyRates, Date currencyDate) {
		Map<String, Boolean> updateStatus = new HashMap<>();
		for (String fromCurr : currencyRates.keySet()) {
			boolean isUpdated = false;
			double rate = currencyRates.get(fromCurr);
			try {
				saveCurrencyRate(fromCurr, currencyTo, rate, currencyDate);
				isUpdated = true;
			} catch (DataIntegrityViolationException ex) {
				logger.error(ex.getMessage());
			}
			updateStatus.put(fromCurr + "=>" + currencyTo , isUpdated);
		}
		return updateStatus;
	}

	/**
	 * in case currency rate service is down, retry after 30 minutes for 5 times.
	 * @param currencyFrom
	 * @param currencyTo
	 * @param rate
	 * @param date
	 */
	@Async
	@Retryable(value = SaveCurrencyRateException.class, maxAttempts = 5, backoff = @Backoff(delay = 30 * 60000))
	//@Retryable(value = SaveCurrencyRateException.class, maxAttempts = 3, backoff = @Backoff(delay = 3000))
	public void saveCurrencyRate(String currencyFrom, String currencyTo, Double rate, Date date) {
		AccFxRateDTO savedAccFxRateDto = callCurrencyRateServiceToSave(currencyFrom,  currencyTo,  rate,  date);
		if(savedAccFxRateDto == null) {
			logger.error("Save obj to database failed. retrying after 30 minutes...");
			throw new SaveCurrencyRateException();
		}
		logger.info("get the returned AccFxRateDTO: " + savedAccFxRateDto);
	}
	
	/**
	 * calling currency rate web services to save the rate to db
	 * @param currencyFrom
	 * @param currencyTo
	 * @param rate
	 * @param date
	 * @return
	 */
	AccFxRateDTO callCurrencyRateServiceToSave(String currencyFrom, String currencyTo, Double rate, Date date) {
		System.out.println("callCurrencyRateServiceToSave... at " + new Date());
		AccFxRateDTO newRate = new AccFxRateDTO(null, dateUtil.getStartOfDay(date), dateUtil.getStartOfDay(date),
				rate, currencyFrom, currencyTo);
		logger.debug("save newRate = " + newRate);
		AccFxRateDTO returned_accFxRateDTO = accFxRateService.saveOrUpdate(newRate);
		return returned_accFxRateDTO;
	}

	private static class SaveCurrencyRateException extends RuntimeException {

	}

}
