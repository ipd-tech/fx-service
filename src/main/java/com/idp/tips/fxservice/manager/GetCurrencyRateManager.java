package com.idp.tips.fxservice.manager;

import java.util.Date;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;

/**
 * this manager is to get the currency rate from reuter site
 * 
 * @author ahg
 *
 */
@Component
public class GetCurrencyRateManager {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${reuterUrl}")
	private String reuterUrl;

	@Autowired
	private RestTemplate restTemplate;
	
	private static String XPATHSTR1 = "//*[@id='content']//div[starts-with(@class,'norm currData')]/text()";
	private static String XPATHSTR2 = "//*[@id='destAmt']/@value";
	private static String[] XPATHSTRS = { XPATHSTR1, XPATHSTR2 };
	
	/**
	 * get the currencyRate from difference xpath, if still cannot get it, then
	 * try again after 15 minutes for 5 times
	 * 
	 * @param srcCurrency
	 * @param destCurrency
	 * @return
	 */
	@Retryable(value = GetCurrencyRateException.class, maxAttempts = 5, backoff = @Backoff(delay = 15*60000))
	public String getCurrencyRate(String srcCurrency, String destCurrency) {
		//System.out.println("calling getCurrencyRate. srcCurrency/destCurrency=" + srcCurrency + "/" + destCurrency);
		String rate = getCurrencyRateFromReuters(srcCurrency, destCurrency);
		
		if (rate == null || rate.trim().length() == 0) { 
			throw new GetCurrencyRateException();
		}
		return rate;
	}

	
	String getCurrencyRateFromReuters(String srcCurrency, String destCurrency) {
		//System.out.println("calling getCurrencyRateFromReuters. srcCurrency/srcCurrency=" + srcCurrency + "/" + destCurrency);
		String url = String.format(reuterUrl, srcCurrency, destCurrency);
		String result = restTemplate.getForObject(url, String.class);
		String rate = null;
		for (String xpathStr : XPATHSTRS) {
//			System.out.println("xpathStr="+xpathStr);
			TagNode tagNode = new HtmlCleaner().clean(result);
			try {
				Document doc = new DomSerializer(new CleanerProperties()).createDOM(tagNode);
				XPath xpath = XPathFactory.newInstance().newXPath();
				rate = (String) xpath.evaluate(xpathStr, doc, XPathConstants.STRING);
				if (rate != null && rate.trim().length() != 0) {
					logger.info("Get the currency rate from " + srcCurrency + " to USD: " + rate + " at " + new Date()
							+ " using xpath: " + xpathStr);
					break;
				}
			} catch (Exception e) {
				logger.warn("Get the currency rate from " + srcCurrency + " to USD rate error from xpathStr "
						+ xpathStr + ": " + e);
			}
		}
		
		return rate;
	}

	private static class GetCurrencyRateException extends RuntimeException {

	}


}
