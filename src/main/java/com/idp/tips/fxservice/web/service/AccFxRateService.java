package com.idp.tips.fxservice.web.service;

import com.idp.tips.fxservice.currencylayer.CurrencyRatesService;
import com.idp.tips.fxservice.dto.AccFxRateDTO;
import com.idp.tips.fxservice.jpa.manager.IAccFxRateManager;
import com.idp.tips.fxservice.manager.SaveCurrencyRateManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.idp.tips.fxservice.util.CacheUtil;
import com.idp.tips.fxservice.TipsDbServiceConstant;

import java.util.Date;
import java.util.Map;

/**
 * 
 * @author ahg
 *
 */
@Component
public class AccFxRateService implements IAccFxRateService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private IAccFxRateManager accFxRateManager;

	@Autowired
	private SaveCurrencyRateManager saveCurrencyRateManager;

	@Autowired
	private CurrencyRatesService currencyRatesService;
	
	@Autowired
	private CacheUtil cacheUtil;
	
	@Cacheable(TipsDbServiceConstant.CACHE_LATESTACCFXRATE)
	public AccFxRateDTO getLatestAccFxRate(String currencyFrom, String currencyTo) {
		//System.out.println("****calling getLatestAccFxRate");
		return accFxRateManager.findLatestRate(currencyFrom, currencyTo);
	}

	public AccFxRateDTO saveOrUpdate(AccFxRateDTO accFxRateDto) {
		AccFxRateDTO accFxRateDTO = accFxRateManager.saveOrUpdate(accFxRateDto);
		if(accFxRateDTO != null){
			cacheUtil.clearLatestRateCaches();
		}
		return accFxRateDTO;
	}
	
	@Cacheable(TipsDbServiceConstant.CACHE_ACCFXRATE)
	public AccFxRateDTO getAccFxRate(Integer idAccFxRate) {
		//System.out.println("****calling getAccFxRate");
		return accFxRateManager.findAccFxRate(idAccFxRate);
	}

	public Map<String, Boolean> updateAccFxRateForDate(Date currencyDate) {
		Map<String, Double> currencyRates = currencyRatesService.getCurrencyRates(currencyDate);
		return saveCurrencyRateManager.saveCurrencyRatesFromMap(currencyRates, currencyDate);
	}

	public Map<String, Boolean> updateAccFxRates() {
		Map<String, Double> currencyRates = currencyRatesService.getCurrencyRates();
		return saveCurrencyRateManager.saveCurrencyRatesFromMap(currencyRates);
	}
}
