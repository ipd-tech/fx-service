package com.idp.tips.fxservice.web.controller;

import com.idp.tips.fxservice.dto.AccFxRateDTO;
import com.idp.tips.fxservice.util.DateUtil;
import com.idp.tips.fxservice.web.service.IAccFxRateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/tips-currencyservice/api/currencyrateservice")
public class AccFxRateController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IAccFxRateService accFxRateService;
	
	@RequestMapping(value = "/latestAccFxRate", method = RequestMethod.GET)
	public AccFxRateDTO getLatestAccFxRate(@RequestParam(value = "from") String from,
										   @RequestParam(value = "to", defaultValue = "USD") String to) throws Exception {
		logger.info("get the request to getLatestAccFxRate");
		return accFxRateService.getLatestAccFxRate(from, to);
	}
	
	@RequestMapping(value = "/accFxRate/{idAccFxRate}", method = RequestMethod.GET)
	public AccFxRateDTO getAccFxRate(@PathVariable Integer idAccFxRate) throws Exception {
		logger.info("get the request to get AccFxRate with id:" + idAccFxRate);
		return accFxRateService.getAccFxRate(idAccFxRate);
	}
	
	@RequestMapping(value = "/saveAccFxRate", method = RequestMethod.POST)
	public AccFxRateDTO saveAccFxRate(@RequestBody AccFxRateDTO accFxRateDTO) throws Exception {
		logger.info("got the request to saveAccFxRate. accFxRateDTO: " + accFxRateDTO);
		AccFxRateDTO returned_accFxRateDTO = accFxRateService.saveOrUpdate(accFxRateDTO);
		logger.info("returned accFxRateDTO: " + returned_accFxRateDTO);
		return returned_accFxRateDTO;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public AccFxRateDTO save(@RequestBody AccFxRateDTO accFxRateDTO) throws Exception {
		return saveAccFxRate(accFxRateDTO);
	}

	@RequestMapping(value = "/updateAccFxRateForDate", method = RequestMethod.GET)
	public Map<String, String> updateAccFxRateForDate(@RequestParam(value = "date") String currencyDate) throws Exception {
		if (currencyDate != null && DateUtil.isDateValid(currencyDate)) {
			Date currencyDateDate = DateUtil.parseDate(currencyDate);
			Map<String, Boolean> updateStatus = accFxRateService.updateAccFxRateForDate(currencyDateDate);
			Map<String, String> updateStatusUserFriendly = updateStatus.entrySet().stream().collect(
													Collectors.toMap(entry -> entry.getKey(),
													entry -> entry.getValue() ? "Successfully Updated" : "Update Failed"));
			return updateStatusUserFriendly;
		} else {
			Map<String, String> errorMap = new HashMap<>();
			errorMap.put("status","failed");
			errorMap.put("reason","No date supplied or invalid date. Date for currency update must be in format yyyy-MM-dd");
			return errorMap;
		}
	}

}
