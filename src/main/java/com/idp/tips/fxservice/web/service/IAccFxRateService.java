package com.idp.tips.fxservice.web.service;


import com.idp.tips.fxservice.dto.AccFxRateDTO;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

public interface IAccFxRateService {

	AccFxRateDTO getLatestAccFxRate(String currencyFrom, String currencyTo);
	AccFxRateDTO getAccFxRate(Integer idAccFxRate);
	AccFxRateDTO saveOrUpdate(AccFxRateDTO accFxRateDto);
	Map<String, Boolean> updateAccFxRateForDate(Date currencyDate);
	Map<String, Boolean> updateAccFxRates();
}
